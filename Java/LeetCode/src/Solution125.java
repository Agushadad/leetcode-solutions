public class Solution125 {
    public boolean isPalindrome(String s) {
        var newString = s.toLowerCase().replaceAll("[^a-zA-Z0-9]", "");
        var index = newString.length() - 1;
        for (int i = 0; i < newString.length() ; i++) {
                if (newString.charAt(i) != newString.charAt(index))
                    return false;
                index--;
            }
        return true;
    }
}
