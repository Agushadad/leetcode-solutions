public class Solution35 {
    public int searchInsert(int[] nums, int target) {
        for(int i = 0; i < nums.length - 1; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[i] >= target)
                    return i;
                else if (nums[j] == target || (target >= nums[i] && target < nums[j]))
                    return j;
            }
        }
        if (nums.length == 1 && target <= nums[0])
            return 0;
        return nums.length;
    }
}
