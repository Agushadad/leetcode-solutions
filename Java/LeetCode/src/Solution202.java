import java.util.ArrayList;
import java.util.List;

public class Solution202 {
    public boolean isHappy(int n) {
        int count = 0;
        while(n != 1) {
            var num = 0;
            var lista = separaNumeros(n);
            for (int i = 0; i < lista.size(); i++) {
                num += Math.pow(lista.get(i), 2);
            }
            n = num;
            count++;
            if (num > 100)
                return false;
        }
        return true;
    }
    private List<Integer> separaNumeros(int n){
        List<Integer> lista = new ArrayList<>();
        while (n != 0) {
            var num = n % 10;
            n = n / 10;
            lista.add(num);
        }
        return lista;
    }
}
