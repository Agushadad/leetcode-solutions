public class Solution9 {
    public boolean isPalindrome(int x) {
        int reversedNum = 0;
        int num = x;
        if(x < 0)
            return false;   
        while(num != 0) {
            reversedNum = reversedNum * 10 + num % 10;
            num = num / 10;
        }
        if(x == reversedNum)
            return true;
        return false;
    }
}
