﻿namespace LeetCode
{
    public class Solution13
    {
        public int RomanToInt(string s)
        {
            int result = 0;
            int index = 1;
            foreach (char i in s)
            {
                var num = Convert_ToRoman(i);
                if (index == s.Length)
                {
                    result += num;
                    return result;
                }
                var prox = Convert_ToRoman(s[index]);
                if (num < prox)
                    num = num * -1;
                result += num;
                index++;
                if (index == s.Length)
                {
                    result += prox;
                    return result;
                }
            }
            return result;
        }
        public int Convert_ToRoman(char i)
        {
            int num = 0;
            if (i == 'I')
                num = 1;
            else if (i == 'V')
                num = 5;
            else if (i == 'X')
                num = 10;
            else if (i == 'L')
                num = 50;
            else if (i == 'C')
                num = 100;
            else if (i == 'D')
                num = 500;
            else if (i == 'M')
                num = 1000;
            return num;
        }
    }
}
